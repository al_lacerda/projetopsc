package DataAcess;

import Entidade.Divida;
import java.util.*;
import javax.persistence.*;

public class DividaDAO {

    private static DividaDAO instance;
    protected EntityManager entityManager;

    public static DividaDAO getInstance() {
        if (instance == null) {
            instance = new DividaDAO();
        }
        return instance;
    }

    private DividaDAO() {
        entityManager = getEntityManager();
    }

    private EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SistemaDebitoPU");
        if (entityManager == null) {
            entityManager = factory.createEntityManager();
        }
        return entityManager;
    }
    
    public Divida getByCodigoCliente(final int codigoCliente) {
        Divida divida= (Divida)entityManager.createQuery("FROM " + Divida.class.getName() + " c where c.codigoCliente = \""+codigoCliente+"\"" +"").getSingleResult();
        return divida;
    }

    @SuppressWarnings("unchecked")
    public List<Divida> findAll() {
        return entityManager.createQuery("FROM " + Divida.class.getName() + " d ").getResultList();
    }

    public void persist(Divida divida) {
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(divida);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void merge(Divida divida) {
        try {
            entityManager.getTransaction().begin();
            entityManager.merge(divida);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void remove(Divida divida) {
        try {
            entityManager.getTransaction().begin();
            divida = entityManager.find(Divida.class, divida.getCodigoCliente());
            entityManager.remove(divida);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void removeByCodigoCliente(final int codigoCliente) {
        try {
            Divida divida = getByCodigoCliente(codigoCliente);
            remove(divida);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
