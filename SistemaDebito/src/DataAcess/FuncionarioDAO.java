package DataAcess;

import Entidade.Funcionario;
import java.util.*;
import javax.persistence.*;

public class FuncionarioDAO {

    private static FuncionarioDAO instance;
    protected EntityManager entityManager;

    public static FuncionarioDAO getInstance() {
        if (instance == null) {
            instance = new FuncionarioDAO();
        }
        return instance;
    }

    private FuncionarioDAO() {
        entityManager = getEntityManager();
    }

    private EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SistemaDebitoPU");
        if (entityManager == null) {
            entityManager = factory.createEntityManager();
        }
        return entityManager;
    }
    
    public Funcionario getById(final int id) {
        return entityManager.find(Funcionario.class, id);
    }

     public Funcionario getByCpf(final String cpf) {
        Funcionario funcionario = (Funcionario) entityManager.createQuery("FROM " + Funcionario.class.getName() + " c where c.cpf = \""+cpf+"\"" +"").getSingleResult();
        return funcionario;
    }
     
    public Funcionario getBySenha(final String senha) {
        Funcionario funcionario = (Funcionario) entityManager.createQuery("FROM " + Funcionario.class.getName() + " c where c.senha = \""+senha+"\"" +"").getSingleResult();
        return funcionario;
    }

    @SuppressWarnings("unchecked")
    public List<Funcionario> findAll() {
        return entityManager.createQuery("FROM " + Funcionario.class.getName() + " c ").getResultList();
    }

    public void persist(Funcionario funcionario) {
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(funcionario);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void merge(Funcionario funcionario) {
        try {
            entityManager.getTransaction().begin();
            entityManager.merge(funcionario);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void remove(Funcionario funcionario) {
        try {
            entityManager.getTransaction().begin();
            funcionario = entityManager.find(Funcionario.class, funcionario.getCodigo());
            entityManager.remove(funcionario);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void removeById(final int id) {
        try {
            Funcionario funcionario = getById(id);
            remove(funcionario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
