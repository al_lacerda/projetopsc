package DataAcess;

import Entidade.Lancamentos;
import java.util.*;
import javax.persistence.*;

public class LancamentosDAO {

    private static LancamentosDAO instance;
    protected EntityManager entityManager;

    public static LancamentosDAO getInstance() {
        if (instance == null) {
            instance = new LancamentosDAO();
        }
        return instance;
    }

    private LancamentosDAO() {
        entityManager = getEntityManager();
    }

    private EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SistemaDebitoPU");
        if (entityManager == null) {
            entityManager = factory.createEntityManager();
        }
        return entityManager;
    }

    public Lancamentos getByCodigoCliente(final int codigoCliente) {
        Lancamentos lancamento = (Lancamentos) entityManager.createQuery("FROM " + Lancamentos.class.getName() + " c where c.codigoCliente = \""+codigoCliente+"\"" +"").getSingleResult();
        return lancamento;
    }

    @SuppressWarnings("unchecked")
    public List<Lancamentos> findAll() {
        return entityManager.createQuery("FROM " + Lancamentos.class.getName() + " c ").getResultList();
    }

    public void persist(Lancamentos lancamento) {
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(lancamento);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void merge(Lancamentos lancamento) {
        try {
            entityManager.getTransaction().begin();
            entityManager.merge(lancamento);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void remove(Lancamentos lancamento) {
        try {
            entityManager.getTransaction().begin();
            lancamento = entityManager.find(Lancamentos.class, lancamento.getCodigoCliente());
            entityManager.remove(lancamento);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void removeByCodigoCliente(final int codigoCliente) {
        try {
            Lancamentos lancamento = getByCodigoCliente(codigoCliente);
            remove(lancamento);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
