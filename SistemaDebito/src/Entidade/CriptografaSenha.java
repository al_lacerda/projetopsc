/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidade;

import java.io.UnsupportedEncodingException;
import java.security.*;

/**
 *
 * @author sillas
 */
public class CriptografaSenha {

    public String criptografar(String senha) throws NoSuchAlgorithmException, UnsupportedEncodingException {

        MessageDigest algotitmo = MessageDigest.getInstance("SHA-256");

        byte messageDigest[] = algotitmo.digest(senha.getBytes("UTF-8"));

        StringBuilder hexString = new StringBuilder();

        for (byte b : messageDigest) {
            hexString.append(String.format("0%2X", 0xFF & b));
        }

        return hexString.toString();
    }
}
